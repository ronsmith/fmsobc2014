# Author: rksmith
# Created: 8/31/14
# Copyright (c)2014 That Ain't Working, All Rights Reserved

import os.path
from flask import Flask, render_template, send_from_directory
from flask.ext.sqlalchemy import SQLAlchemy
from werkzeug.contrib.fixers import ProxyFix
from config import DB_URI


app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = DB_URI
db = SQLAlchemy(app)


static_dir = os.path.join(app.root_path, 'static')


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(static_dir, 'favicon.ico', mimetype='image/vnd.microsoft.icon')


# for running stand-alone server
if __name__ == '__main__':
    from config import HOST, PORT
    app.run(host=HOST, port=PORT, debug=False, threaded=True)

else:
    app.wsgi_app = ProxyFix(app.wsgi_app)


