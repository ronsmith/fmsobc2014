#!/bin/bash

virtualenv -p python3 venv
. ./venv/bin/activate

python3 -V
which python3

while read -ep "Is the python version and location correct? [Y/n]: " correctpy; do
    case ${correctpy} in
        [Yy])   break ;;
        '')     break ;;
        [Nn])   exit ;;
        *)      ;;
    esac
done

pip3 install -r requirements.txt

pip3 list
python3 -V
which python3
