# Author: Ron Smith
# Created: 8/31/14
# Copyright (c)2014 That Ain't Working, All Rights Reserved

from fmsobc import db


class Person(db.Model):
    __tablename__ = 'person'

    id = db.Column(db.Integer, primary_key=True)
    role = db.Column(db.Enum('director', 'instructor', 'officer', 'booster', name='person_roles'))
    name = db.Column(db.String(200))
    title = db.Column(db.String(100))
    bio = db.Column(db.Text)
    photo_url = db.Column(db.String(200))
    display_order = db.Column(db.Integer)
    is_active = db.Column(db.Boolean)


db.Index('ix_person_role', db.metadata.tables['person'].c.role)


