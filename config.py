# Author: rksmith
# Created: 8/31/14
# Copyright (c)2014 That Ain't Working, All Rights Reserved

import os.path
import logging.config
from thataintworking.mail import MailServer

DB_URI = 'postgresql://{uid}:{pwd}@fmsobcdb.ccnelnxaqxhn.us-east-1.rds.amazonaws.com:5432/fmsobc'.format(
    uid='fmsobcdev',
    # uid='fmsobctest',
    # uid='fmsobcprod',
    pwd='orch4fun'
)

MAIL_SERVER = MailServer('email-smtp.us-east-1.amazonaws.com', default_from='noreply@forestwoodorchestra.org')

##### STANDALONE SERVER CONFIG #####

HOST = '0.0.0.0'
PORT = 5001

##### CACHE CONFIGURATION #####

CACHE_SERVERS = ['unix:/tmp/memcached.sock']
CACHE_PREFIX = 'FO'
CACHE_TIMEOUT = 3600  # one hour

##### LOGGING CONFIGURATION #####

_APP_DIR = os.path.abspath(os.path.dirname(__file__))
_LOG_FILENAME = os.path.abspath(os.path.join(_APP_DIR, 'logs', 'fmsobc.log'))

logging.config.dictConfig({
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s',
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'level': 'DEBUG',
            'formatter': 'standard',
            'stream': 'ext://sys.stdout',
        },
        'file': {
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'level': 'DEBUG',
            'formatter': 'standard',
            'filename': _LOG_FILENAME,
            'when': 'midnight',
            'backupCount': 10,
            'encoding': 'utf8',
        },
    },
    'loggers': {
        'flask': {
            'handlers': ['console', 'file'],
            'propagate': True,
            'level': 'INFO',
        },
        'werkzeug': {
            'handlers': ['console', 'file'],
            'propagate': True,
            'level': 'INFO',
        },
        '': {
            'handlers': ['console', 'file'],
            'level': os.environ.get('LOG_LEVEL', 'DEBUG'),
            'propagate': True,
        },
    },
})
