#!/bin/sh

if [ -z "$1" ]; then
    v=`date "+%Y-%m-%d"`
else
    v=$1
fi

pkgname='fmsobc'
tempdir=`mktemp -d /tmp/${pkgname}.XXXXXXXXXX`
pkgdir=${tempdir}/${pkgname}_${v}

mkdir -p ${pkgdir}
cp -R `ls | grep -v "venv$" | grep -v tar.gz | grep -v package.sh` ${pkgdir}
rm -f `find ${pkgdir} | grep ".*\.pyc$"`
rm -f ${pkgdir}/logs/*
tar czfC ${pkgname}_${v}.tar.gz ${pkgdir}/.. ${pkgname}_${v}
rm -rf ${tempdir}

