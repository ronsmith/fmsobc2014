#!/bin/bash
set -e
LOGFILE=/var/log/gunicorn/middletier.log
LOGDIR=$(dirname $LOGFILE)
NUM_WORKERS=2
# user/group to run as
USER=mtr
GROUP=mtr
ADDRESS=unix:/tmp/middletier.sock

cd /opt/MiddleTier
source venv/bin/activate
test -d $LOGDIR || mkdir -p $LOGDIR
exec gunicorn MiddleTier:app \
	-w $NUM_WORKERS \
	-b $ADDRESS \
	-t 100 \
	--user=$USER \
	--group=$GROUP \
	--log-level=info \
	--log-file=$LOGFILE \
	--env LOG_LEVEL=DEBUG \
	2>>$LOGFILE
