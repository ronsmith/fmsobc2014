create user fmsobcprod password 'orch4fun';
create user fmsobctest password 'orch4fun';
create user fmsobcdev password 'orch4fun';

grant fmsobcprod to dbmaster;
grant fmsobctest to dbmaster;
grant fmsobcdev to dbmaster;

create schema prod authorization fmsobcprod;
create schema test authorization fmsobctest;
create schema dev authorization fmsobcdev;

alter role fmsobcprod set search_path=prod;
alter role fmsobctest set search_path=test;
alter role fmsobcdev set search_path=dev;

