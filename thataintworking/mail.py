# Author: Ron Smith
# Created: 8/31/14
# Copyright (c)2014 That Ain't Working, All Rights Reserved

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import smtplib


class RequiredDataException(Exception):
    pass


class Attachment:

    def __init__(self, text, filename):
        self.mime_text = MIMEText(text)
        self.mime_text.add_header('Content-Disposition', 'attachment', filename=filename)


class MailServer:

    def __init__(self, host, port=0, default_from=None, userid=None, password=None, ssl=False, tls=False):
        if not host:
            raise RequiredDataException('host is required')
        self.host = host
        self.port = port
        self.default_from = default_from
        self.userid = userid
        self.password = password
        self.ssl = ssl
        self.tls = tls

    def sendmail(self, to, frm=None, subj='', text=None, html=None, attachments=None):
        if not to:
            raise RequiredDataException('to is require')

        if not frm:
            frm = self.default_from
        if not frm:
            raise RequiredDataException('frm is required if def_frm was not set')

        if text and html:
            msg = MIMEMultipart('alternative')
            msg.attach(MIMEText(text, 'plain'))
            msg.attach(MIMEText(html, 'html'))
        elif text:
            msg = MIMEText(text, 'plain')
        elif html:
            msg = MIMEText(html, 'html')
        else:
            raise RequiredDataException('At least one of text or html is required.')

        if attachments:
            for a in attachments:
                msg.attach(a.mime_text)

        msg['Subject'] = subj
        msg['From'] = frm
        msg['To'] = to

        if self.ssl:
            s = smtplib.SMTP_SSL(self.host, self.port)
        else:
            s = smtplib.SMTP(self.host, self.port)
        if self.tls:
            s.starttls()
        if self.userid and self.password:
            s.login(self.userid, self.password)

        s.sendmail(frm, to, msg.as_string())
        s.quit()
